const Express   = require('express')
const args      = require('minimist')(process.argv.slice(2))
const winston   = require('winston')
const path      = require('path')
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const consoleTransport = new winston.transports.Console()
const app = Express()
const port = process.env.PORT ? parseInt(process.env.PORT) : 12435
const logger = new winston.createLogger({
    transports: [consoleTransport]
})
const remoteKey = process.env.KEY || 'KEY_HERE'
 
// const needThese = ['key', 'path', 'name']

// if (!args.key || !args.path) {
//     console.error('Parameters path, name and key are mandatory')
//     process.exit()
// }

// const absolutePath = path.resolve(args.path)

function logRequest(req, res, next) {
    logger.info({
        query   : req.query,
        date    : new Date(),
        headers : req.headers
    })
    next()
}
app.use(logRequest)

function logError(err, req, res, next) {
    logger.error(err)
    next()
}
app.use(logError)

app.get('/run', (req, res, next) => {
    if (!req.headers['x-gitlab-token']) throw('Need Key')
    if (req.headers['x-gitlab-token'] !== remoteKey) throw('wrong key')
    const cmds = JSON.parse(req.query.commands)
    cmds.forEach(async element => {
        try {
            const { stdout, stderr } = await exec(element);
            logger.info(stdout)
        }
        catch (e) {
            // logger.error('Command Error')    
        }
    })
    res.send('ok')
})
app.listen(port, () => console.log(`Pull & Build server started on http://localhost:${port}`))